#!/usr/bin/env node

const commands = require("../lib/commands");
const { CommandExecutor } = require("../lib/classes/command-executor");

const executor = new CommandExecutor(commands);
executor.run();
