import { CommandExecutor, ShellCommands } from "./classes/command-executor";
import { AWSContainer } from "./components/aws/aws";
import { Python } from "./components/python";
import { AWS_Appsync } from "./components/appsync/appsync";
import { Env } from "./components/env";

export async function routine(routineFunction: () => Promise<void>) {
  await routineFunction();
}

export { config as defaultconfig } from "./config";
export { logger } from "./classes/logger";

class Module {
  private _python: Python | undefined = undefined;
  public get python() {
    if (!this._python) {
      this._python = new Python({});
    }
    return this._python;
  }

  private _env: Env | undefined = undefined;
  public get env() {
    if (!this._env) {
      this._env = new Env();
    }
    return this._env;
  }

  private _appsync: AWS_Appsync | undefined = undefined;
  public get appsync() {
    if (!this._appsync) {
      this._appsync = new AWS_Appsync();
    }
    return this._appsync;
  }

  private _aws: AWSContainer | undefined = undefined;
  public get aws() {
    if (!this._aws) {
      this._aws = new AWSContainer();
    }
    return this._aws;
  }
}

export function commands(commands: ShellCommands<any>) {
  const executor = new CommandExecutor(commands);
  executor.run();
}

const module = new Module();
export default module;
