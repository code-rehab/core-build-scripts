import { ProcessParams, ShellCommand } from "../classes/command-executor";
import { Python } from "../components/python";

export interface PythonCommands {
  build: ShellCommand;
}

export function pythonCommands(): PythonCommands {
  const python = new Python({});
  return {
    // build python package
    build: async ({ source, destination, name, dependencies }: ProcessParams) =>
      await python.build(source, destination, name, Boolean(dependencies))
  };
}
