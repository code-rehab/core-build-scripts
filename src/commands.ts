import { awsCommands, AWSCommands } from './commands/aws';
import { pythonCommands, PythonCommands } from './commands/python';

export interface PentairCommands {
  python: PythonCommands;
  aws: AWSCommands;
}

export const python = pythonCommands();
export const aws = awsCommands();
