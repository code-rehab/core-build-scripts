import { logger } from './logger';

export type ProcessParams = Record<string, string>;
export type ProcessCommandNames = string[];
export type ProcessCommandResponse = Promise<void | string | undefined>;
export type ShellCommands<CommandNames> = { [name in keyof CommandNames]: ShellCommand | ShellCommands<string> };
export type ShellCommand = (params: ProcessParams, commands: ProcessCommandNames) => ProcessCommandResponse;

export class CommandExecutor {
  private _params: ProcessParams = {};
  private _processCommands: ProcessCommandNames = [];
  private _currentCommand: ShellCommand;

  constructor(private _commands: ShellCommands<any>, private _args: string[] = process.argv.slice(2)) {
    this._parseArgs();
    this._currentCommand = this._findCommand();
  }

  public run = () => {
    this.printResponse(this._currentCommand(this._params, this._processCommands));
  };

  private _findCommand = (): ShellCommand => {
    let command = this._processCommands.reduce((commands: any, name: string, index: number) => {
      return (commands && commands[name]) || this.commandUnknown;
    }, this._commands);

    const commandRequired = typeof command === "object";
    return commandRequired ? this.commandRequired(command) : command;
  };

  private _parseArgs = () => {
    this._args
      .filter(arg => arg.substr(0, 2) === "--")
      .reduce((params: Record<string, any>, arg: string) => {
        const parts = arg.substr(2).split("=");
        params[parts[0]] = parts[1] || true;

        return params;
      }, this._params);

    this._processCommands = this._args.filter(arg => arg.substr(0, 2) !== "--");
  };

  public commandUnknown = (_params: ProcessParams, _processCommands: ProcessCommandNames) => {
    logger.error("Command unknown");
  };

  public commandRequired = (commands: ShellCommands<any>) => {
    return async (_params: ProcessParams, _processCommands: ProcessCommandNames) => {
      logger.error(
        "Command required.\nPlease include one of the following commands: \n - " + Object.keys(commands).join("\n - ")
      );
    };
  };

  public printResponse = async (command?: Promise<string | undefined | void>) => {
    const response = await command;
    if (response) {
      logger.info(response);
    }
  };
}
