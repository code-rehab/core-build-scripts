import * as winston from 'winston';

import { config } from '../config';

const { combine, simple, colorize, printf } = winston.format;
const level = config.logger.level;

export const logger: winston.Logger = winston.createLogger({
  level: level,

  format: combine(
    simple(),
    colorize({ all: true }),
    printf(({ message, level }) =>
      level === "error" ? `\n${message}\n` : `${message}`
    )
  ),
  defaultMeta: undefined,
  transports: [new winston.transports.Console()]
});
