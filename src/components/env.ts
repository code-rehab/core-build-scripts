import { existsSync, unlinkSync, createWriteStream } from "fs";
import { AWSContainer } from "./aws/aws";
import { logger } from "../classes/logger";

interface EnvCreateConfig {
  file: string;
  values: Record<string, string>;
}

export class Env {
  public config: EnvCreateConfig = { file: ".env", values: {} };
  private _aws: AWSContainer | undefined = undefined;

  public get aws() {
    if (!this._aws) {
      this._aws = new AWSContainer();
    }
    return this._aws;
  }

  public create(config: EnvCreateConfig) {
    this.config = config;
    this.fetchValues();
  }

  private async fetchValues() {
    return new Promise(async (resolve, reject) => {
      const configValues = this.config.values;
      if (existsSync(this.config.file)) {
        logger.info(`Deleting existing config file: ${this.config.file}`);
        unlinkSync(this.config.file);
      }
      var envfile = createWriteStream(this.config.file, {
        flags: "a" // 'a' means appending (old data will be preserved)
      });
      for (const item in configValues) {
        const value = configValues[item].split("::");
        const source = value[0];
        const property = value[1];
        var result;

        switch (source) {
          // now check where values have to come from
          case "cfn": //cloudformation export values
            result = await this.aws.getCFValue(property);
            break;

          case "awssdk":
            switch (property) {
              case "region":
                result = "";
                break;
              default:
                result = "";
                break;
            }
            break;

          case "fixed":
            result = property;
            break;

          default:
            logger.error(`Value not set: ${configValues[item]}`);
            result = configValues[item];
            break;
        }

        envfile.write(`${item}=${result}\n`);
        logger.info(`Value set: ${item}=${result}`);
      }
      resolve();
    });
  }
}
