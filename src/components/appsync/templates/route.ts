import { AppsyncRouteConfig } from "../appsync-data";
import { AppsyncTemplate } from "../appsync";

export const route_templates: AppsyncTemplate = {
  request: {
    resolve: ({ route }: AppsyncRouteConfig) => `
$util.qr($ctx.stash.put("route", "${route}"))
$util.qr($ctx.stash.put("operation", "Invoke"))

{}
`,
    resolveBatch: ({ route, field }: AppsyncRouteConfig) => `
$util.qr($ctx.stash.put("route", "${route}"))
$util.qr($ctx.stash.put("operation", "BatchInvoke"))
$util.qr($ctx.stash.put("field", "${field}"))

{}
`,
  },

  response: {
    resolve: () => `$utils.toJson($context.result)`,
    resolveBatch: () => `$utils.toJson($context.result)`,
  },
};
