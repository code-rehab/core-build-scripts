import { SchemaDirectiveVisitor, Transform } from "graphql-tools";
import { GraphQLObjectType, GraphQLSchema, GraphQLID } from "graphql";
import { schemaData, AppsyncRouteConfig } from "../appsync-data";
import { DefaultTransformer } from "../classes/transformer";

enum ResourceFunction {
  Get = "get",
  List = "list",
  Create = "create",
  Update = "update",
  Delete = "delete",
}

export class ModelDirective extends SchemaDirectiveVisitor {
  public static typeDef() {
    return `directive @model(resolver:String, queries: String) on OBJECT`;
  }

  public static transformer() {
    return new ModelTransformer();
  }

  public createRoute(functionName: ResourceFunction, type: GraphQLObjectType): AppsyncRouteConfig {
    return {
      resolver: this.args.resolver,
      route: type.name.toLowerCase() + "." + functionName,
      template: { type: "route", use: "resolve" },
      field: "",
      type: type,
    };
  }

  public visitObject(object: GraphQLObjectType) {
    const name = object.name;

    schemaData.models.insert(name, object);
    schemaData.mapping
      .insert("Query." + name, this.createRoute(ResourceFunction.Get, object))
      .insert("Query." + name + "Collection", this.createRoute(ResourceFunction.List, object))
      .insert("Mutation.create" + name, this.createRoute(ResourceFunction.Create, object))
      .insert("Mutation.update" + name, this.createRoute(ResourceFunction.Update, object))
      .insert("Mutation.delete" + name, this.createRoute(ResourceFunction.Delete, object));
  }
}

class ModelTransformer extends DefaultTransformer implements Transform {
  public transformSchema(schema: GraphQLSchema) {
    this._schema = schema;
    this._config = schema.toConfig();

    let mutationFields: Record<string, any> = {};
    let queryFields: Record<string, any> = {};

    schemaData.models.toArray().forEach((model) => {
      const name = model.name;

      if (!this._config!.query!.getFields()[name]) {
        queryFields[name] = { type: name, arguments: { id: { type: GraphQLID, required: true } } };
      }

      if (!this._config!.query!.getFields()["all" + name + "s"]) {
        queryFields[name + "Collection"] = { type: name, list: true };
      }

      if (!this._config!.mutation!.getFields()["create" + name]) {
        mutationFields["create" + name] = {
          type: name,
          arguments: { input: { type: "input" + name, required: true } },
        };
      }

      if (!this._config!.mutation!.getFields()["update" + name]) {
        mutationFields["update" + name] = {
          type: name,
          arguments: { input: { type: "input" + name, required: true } },
        };
      }

      if (!this._config!.mutation!.getFields()["delete" + name]) {
        mutationFields["delete" + name] = {
          type: name,
          arguments: { id: { type: GraphQLID, required: true } },
        };
      }
    });

    this.addFields("Mutation", mutationFields);
    this.addFields("Query", queryFields);

    return new GraphQLSchema(this._config);
  }
}
