import { makeExecutableSchema } from "graphql-tools";
import { schemaData } from "../../appsync-data";
import { ModelDirective } from "../model";

const typeDefs = ModelDirective.typeDef();

test("@model should add routes to schemaData", () => {
  makeExecutableSchema({
    typeDefs:
      typeDefs +
      `  
      type User @model {
        name: String,
        description: String
      }
      `,
    schemaDirectives: {
      model: ModelDirective
    }
  });

  const result = schemaData.mapping.get();

  expect(JSON.stringify(result["Mutation.createUser"])).toBe(
    '{"route":"user.create","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );

  expect(JSON.stringify(result["Mutation.deleteUser"])).toBe(
    '{"route":"user.delete","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );

  expect(JSON.stringify(result["Mutation.updateUser"])).toBe(
    '{"route":"user.update","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );

  expect(JSON.stringify(result["Query.User"])).toBe(
    '{"route":"user.get","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );

  expect(JSON.stringify(result["Query.UserCollection"])).toBe(
    '{"route":"user.list","template":{"type":"route","use":"resolve"},"field":"","type":"User"}'
  );
});
