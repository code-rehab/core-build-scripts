import {
  GraphQLSchema,
  GraphQLSchemaConfig,
  GraphQLNamedType,
  GraphQLObjectType,
  GraphQLList,
  assertListType,
  assertOutputType,
  assertInputType,
  GraphQLFieldConfigArgumentMap,
  assertInputObjectType,
  GraphQLNonNull
} from "graphql";
import Maybe from "graphql/tsutils/Maybe";

export class DefaultTransformer {
  protected _schema: GraphQLSchema | undefined;
  protected _config: GraphQLSchemaConfig & { types: any } | undefined;

  public pluckType(typename: string) {
    if (this._config) {
      return this._config.types.splice(
        this._config.types.findIndex((type: GraphQLNamedType) => type.name === typename)
      )[0];
    }
  }

  protected addFields(typename: string, fields: Record<string, any>) {
    const type: Maybe<GraphQLObjectType> = this.pluckType(typename);

    if (!this._config || !this._schema) {
      return;
    }

    if (type) {
      const typeConfig = type.toConfig();

      Object.keys(fields).forEach(key => {
        const cfg = fields[key];
        const ReturnType = this._schema!.getType(cfg.type);
        const args: GraphQLFieldConfigArgumentMap = {};

        Object.keys(cfg.arguments || {}).forEach(argname => {
          const argcfg = cfg.arguments[argname];
          let ObjectType;

          if (typeof argcfg.type === "string") {
            ObjectType = this._schema!.getType(argcfg.type);
          }

          if (ObjectType) {
            const input = assertInputObjectType(ObjectType);
            const t = argcfg.required ? new GraphQLNonNull(input) : input;
            args[argname] = { type: t };
          } else {
            const input = assertInputType(argcfg.type);
            const t = argcfg.required ? new GraphQLNonNull(input) : input;
            args[argname] = { type: t };
          }
        });

        if (ReturnType) {
          const ListType = new GraphQLList(ReturnType);
          typeConfig.fields[key] = {
            type: cfg.list ? assertListType(ListType) : assertOutputType(ReturnType),
            args
          };
        }
      });

      if (typename === "Query") {
        this._config.query = new GraphQLObjectType(typeConfig);
      } else if (typename === "Mutation") {
        this._config.mutation = new GraphQLObjectType(typeConfig);
      } else if (typename === "Subscription") {
        this._config.subscription = new GraphQLObjectType(typeConfig);
      } else {
        this._config.types.push(new GraphQLObjectType(typeConfig));
      }
    }
  }
}
