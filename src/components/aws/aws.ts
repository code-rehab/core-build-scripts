import { CloudFormation, config } from "aws-sdk";

import { logger } from "../../classes/logger";
import { AWS_Appsync } from "../appsync/appsync";
import { AWS_Cognito } from "./cognito";
import { AWS_S3 } from "./s3";

// config.update({ credentials: new SharedIniFileCredentials() });

export class AWSContainer {
  private _config: any = {};
  private _s3: AWS_S3 | undefined = undefined;
  private _cognito: AWS_Cognito | undefined = undefined;
  private _appsync: AWS_Appsync | undefined = undefined;
  // public region: string = "us-east-1";

  constructor(region: string = "us-east-1", profile: string = "personal") {
    // config.update({ region: region });
    // console.log(config.credentials);
    // this.region = region;
  }

  // Create or get an AWS_S3 instance
  public get s3(): AWS_S3 {
    if (!this._s3) {
      this._s3 = new AWS_S3(this);
    }
    return this._s3;
  }

  // Create or get an AWS_Cognito instance
  public get cognito(): AWS_Cognito {
    if (!this._cognito) {
      this._cognito = new AWS_Cognito(this);
    }
    return this._cognito;
  }

  // Create or get an AWS_Cognito instance
  public get appsync(): AWS_Appsync {
    if (!this._appsync) {
      this._appsync = new AWS_Appsync(this);
    }
    return this._appsync;
  }

  // get value from Cloudformation stacks
  public getCFValue = async (key: string) => {
    return new Promise((resolve, reject) => {
      const cfn = new CloudFormation();
      cfn.listExports({}, (error, data) => {
        if (error) {
          logger.error(
            `Error during AWS.CloudFormation.listExports ! ${error.message}`
          );

          process.exit(1);
        }

        const result =
          data &&
          data.Exports &&
          data.Exports.filter(_export => _export.Name === key);

        // check if the export exists
        if (!result || result.length !== 1) {
          reject(
            `Cloudformation export ${key} not found, Please initialize your account before deploying this package`
          );
        } else {
          resolve(result[0].Value);
        }
      });
    });
  };
}
