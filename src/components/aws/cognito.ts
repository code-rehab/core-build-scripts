import { CognitoIdentityServiceProvider } from "aws-sdk";
import { CognitoOptions } from "aws-sdk/clients/es";
import { UserAttributeList } from "aws-sdk/clients/inspector";
import { spawn } from "child_process";

import { logger } from "../../classes/logger";
import { requireProps } from "../../helpers";
import { AWSContainer } from "./aws";

export interface UserConfig {
  username: string;
  group?: string;
  accessID?: string;
  attributes?: Record<string, string>;
}

export class AWS_Cognito {
  private _temporaryPass = "TemporaryPass@01";
  private _finalPass = "Pentair@01";
  private _pooldata: any;
  private _serviceProvider: CognitoIdentityServiceProvider;

  constructor(private _aws: AWSContainer) {
    this._serviceProvider = new CognitoIdentityServiceProvider();
  }

  private _pool = async (config: any) => {
    if (!this._pooldata) {
      const id = await this._aws.getCFValue(config.id);
      const clientID = await this._aws.getCFValue(config.clientID);

      this._pooldata = {
        id: id,
        clientID: clientID
      };
    }

    return this._pooldata;
  };

  public async createUsers(config: any): Promise<void> {
    const pool = await this._pool(config.pool);

    await this._removeUsers(config, pool);

    await Promise.all(
      config.users.map(async (user: any) => {
        user = await this._createUser(user, pool);
        await this.addUserToGroup(user, pool);
        await this.resetUserPassword(user, pool);
      })
    );
  }

  public async removeUsers(config: any): Promise<void> {
    this._removeUsers(config);
  }

  public async _removeUsers(config: any, pool?: any) {
    pool = pool || (await this._pool(config.pool));

    return Promise.all(
      (config.users || []).map(async (user: UserConfig) => {
        return await this._removeUser(user, pool);
      })
    );
  }

  private async _removeUser(user: UserConfig, pool: any) {
    const params = {
      UserPoolId: pool.id,
      Username: user.username
    };

    logger.info(`Removing user "${user.username}" in cognito pool: "${pool.id}"`);

    return new Promise(async (resolve, reject) => {
      this._serviceProvider.adminGetUser(params, (err, data) => {
        if (!err && data) {
          this._serviceProvider.adminDeleteUser({ ...params, Username: data.Username }, (err, data) =>
            err ? reject(err) : resolve(data)
          );
        } else {
          resolve(data);
        }
      });
    });
  }

  private async _createUser(config: UserConfig, pool: any) {
    const attributes = config.attributes || {};
    const params = {
      Username: config.username,
      UserAttributes: [
        ...Object.keys(attributes).map(key => ({
          Name: key,
          Value: attributes[key]
        })),
        { Name: "email_verified", Value: "true" }
      ],
      DesiredDeliveryMediums: ["EMAIL"],
      TemporaryPassword: this._temporaryPass,
      UserPoolId: pool.id
    };

    logger.info(`Creating user "${config.username}" in cognito pool: "${pool.id}"`);

    return new Promise(async (resolve, reject) => {
      this._serviceProvider.adminCreateUser(params, async (err, data) => {
        if (err) {
          logger.error(`${err.stack}`);
          process.exit(1);
        }
        resolve({
          ...config,
          username: data.User && data.User.Username
        });
      });
    });
  }

  async addUserToGroup(user: UserConfig, pool: any) {
    const params = {
      GroupName: user.group || "" /* required */,
      UserPoolId: pool.id /* required */,
      Username: user.username /* required */
    };

    requireProps(params);

    return new Promise(async (resolve, reject) => {
      this._serviceProvider.adminAddUserToGroup(params, (err, data) => {
        if (err) {
          logger.error(`${err.stack}`);
          process.exit(1);
        }
        resolve(data);
      });
    });
  }

  async getUserToken(user: UserConfig, pool: any, callback: (data: string) => any) {
    // User Login

    const login = spawn(
      `aws cognito-idp initiate-auth \
        --client-id ${pool.clientID} \
        --auth-flow USER_PASSWORD_AUTH \
        --auth-parameters \
        --region us-east-1 \
        USERNAME=${user.username},PASSWORD=${this._temporaryPass}`,
      { shell: "/bin/bash" }
    );

    login.on("close", code => {
      if (code !== 0) {
        logger.error(`Command failed with exit code ${code}.`);
        process.exit(1);
      } else {
        callback(databuffer);
      }
    });

    // Use session token to complete login and set new password
    let databuffer = "";
    login.stdout.on("data", data => {
      databuffer += data;
    });

    // print live stderr data to the logger
    login.stderr.on("data", data => {
      logger.error(`stderr: ${data}`);
    });
  }

  public async resetUserPassword(user: any, pool: any) {
    return new Promise((resolve, reject) => {
      this.getUserToken(user, pool, (data: any) => {
        logger.info(`Challenging Password for ${user.attributes.email}`);
        data = JSON.parse(data);
        const changePass = spawn(
          `aws cognito-idp admin-respond-to-auth-challenge \
        --user-pool-id ${pool.id} \
        --client-id ${pool.clientID} \
        --challenge-responses \
        --region us-east-1 \
        "NEW_PASSWORD=${this._finalPass},USERNAME=${user.username}" \
        --challenge-name NEW_PASSWORD_REQUIRED \
        --session ${data.Session}`,
          { shell: "/bin/bash" }
        );

        changePass.on("close", function(code) {
          if (code !== 0) {
            logger.error(`Command failed with exit code ${code}.`);
            process.exit(1);
          }
        });

        changePass.stdout.on("data", () => {
          resolve();
        });

        changePass.stderr.on("data", data => {
          logger.error(`stderr: ${data}`);
          reject();
        });
      });
    });
  }
}
