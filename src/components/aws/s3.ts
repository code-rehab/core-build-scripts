import { ProcessParams, ShellCommands } from "../../classes/command-executor";
import { logger } from "../../classes/logger";
import { requireProps } from "../../helpers";
import { AWSContainer } from "./aws";

const { spawn } = require("child_process");

export class AWS_S3 {
  private _config: any = {};

  constructor(private _aws: AWSContainer) {
    // this.config = cfg.create(config);
  }

  public async pushAll(config: any[]) {
    config.forEach((cfg: any) => this.push(cfg.bucketRef, cfg.source, cfg.location));
  }

  public async pullAll(config: any[]) {
    config.forEach((cfg: any) => this.push(cfg.bucketRef, cfg.source, cfg.location));
  }

  public async pull(bucketRef: string, source: string, location: string = "") {
    requireProps({ bucketRef, source, location });

    const bucketName = await this._aws.getCFValue(bucketRef);
    await this._syncbucket(`${source}/${location}`, `s3://${bucketName}/${location}`);

    logger.info(`Succesfully pulled data from s3://${bucketName}/${location} to ${source}/${location}`);
  }

  public async push(bucketRef: string, source: string, location: string = "") {
    requireProps({ bucketRef, source, location });

    const bucketName = await this._aws.getCFValue(bucketRef);

    await this._syncbucket(`${source}/${location}`, `s3://${bucketName}/${location}`);

    logger.info(`Succesfully pushed data from ${source}/${location} to s3://${bucketName}/${location}`);
  }

  private async _syncbucket(source: string, destination: string) {
    return new Promise((resolve, reject) => {
      logger.info("Syncing");
      const command = `aws s3 sync ${source} ${destination}`;
      const child = spawn(command, { shell: "/bin/bash" });

      child.on("close", function(code: number) {
        if (code !== 0) {
          logger.error(`Command failed with exit code ${code}.`);
          process.exit(1);
        }
        resolve();
      });

      // print live stdout data to the console
      child.stdout.on("data", (data: any) => {
        process.stdout.write(`${data}`);
      });

      // print live stderr data to the console
      child.stderr.on("data", (data: any) => {
        process.stdout.write(`${data}`);
        console.log("tr");
      });
    });
  }
}
