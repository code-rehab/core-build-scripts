export declare class Python {
    private _config;
    constructor(config: any);
    buildDev(source: string, destination?: string, artifact_name?: string): Promise<void>;
    build(source: string, destination: string | undefined, name: string | undefined, dependencies: boolean): Promise<void>;
    format(): Promise<unknown>;
    test_source(): Promise<unknown>;
    build_dependencies(destination?: string): Promise<unknown>;
    build_source(source: string, destination: string): Promise<unknown>;
    create_artifact(source: string, name: string): Promise<unknown>;
}
