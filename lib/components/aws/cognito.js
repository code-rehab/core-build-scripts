"use strict";
var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value);
            }).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y["return"]
                  : op[0]
                  ? y["throw"] || ((t = y["return"]) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = require("aws-sdk");
var child_process_1 = require("child_process");
var logger_1 = require("../../classes/logger");
var helpers_1 = require("../../helpers");
var AWS_Cognito = /** @class */ (function() {
  function AWS_Cognito(_aws) {
    var _this = this;
    this._aws = _aws;
    this._temporaryPass = "TemporaryPass@01";
    this._finalPass = "Pentair@01";
    this._pool = function(config) {
      return __awaiter(_this, void 0, void 0, function() {
        var id, clientID;
        return __generator(this, function(_a) {
          switch (_a.label) {
            case 0:
              if (!!this._pooldata) return [3 /*break*/, 3];
              return [4 /*yield*/, this._aws.getCFValue(config.id)];
            case 1:
              id = _a.sent();
              return [4 /*yield*/, this._aws.getCFValue(config.clientID)];
            case 2:
              clientID = _a.sent();
              this._pooldata = {
                id: id,
                clientID: clientID
              };
              _a.label = 3;
            case 3:
              return [2 /*return*/, this._pooldata];
          }
        });
      });
    };
    this._serviceProvider = new aws_sdk_1.CognitoIdentityServiceProvider();
  }
  AWS_Cognito.prototype.createUsers = function(config) {
    return __awaiter(this, void 0, void 0, function() {
      var pool;
      var _this = this;
      return __generator(this, function(_a) {
        switch (_a.label) {
          case 0:
            return [4 /*yield*/, this._pool(config.pool)];
          case 1:
            pool = _a.sent();
            return [4 /*yield*/, this._removeUsers(config, pool)];
          case 2:
            _a.sent();
            return [
              4 /*yield*/,
              Promise.all(
                config.users.map(function(user) {
                  return __awaiter(_this, void 0, void 0, function() {
                    return __generator(this, function(_a) {
                      switch (_a.label) {
                        case 0:
                          return [4 /*yield*/, this._createUser(user, pool)];
                        case 1:
                          user = _a.sent();
                          return [4 /*yield*/, this.addUserToGroup(user, pool)];
                        case 2:
                          _a.sent();
                          return [
                            4 /*yield*/,
                            this.resetUserPassword(user, pool)
                          ];
                        case 3:
                          _a.sent();
                          return [2 /*return*/];
                      }
                    });
                  });
                })
              )
            ];
          case 3:
            _a.sent();
            return [2 /*return*/];
        }
      });
    });
  };
  AWS_Cognito.prototype.removeUsers = function(config) {
    return __awaiter(this, void 0, void 0, function() {
      return __generator(this, function(_a) {
        this._removeUsers(config);
        return [2 /*return*/];
      });
    });
  };
  AWS_Cognito.prototype._removeUsers = function(config, pool) {
    return __awaiter(this, void 0, void 0, function() {
      var _a;
      var _this = this;
      return __generator(this, function(_b) {
        switch (_b.label) {
          case 0:
            _a = pool;
            if (_a) return [3 /*break*/, 2];
            return [4 /*yield*/, this._pool(config.pool)];
          case 1:
            _a = _b.sent();
            _b.label = 2;
          case 2:
            pool = _a;
            return [
              2 /*return*/,
              Promise.all(
                (config.users || []).map(function(user) {
                  return __awaiter(_this, void 0, void 0, function() {
                    return __generator(this, function(_a) {
                      switch (_a.label) {
                        case 0:
                          return [4 /*yield*/, this._removeUser(user, pool)];
                        case 1:
                          return [2 /*return*/, _a.sent()];
                      }
                    });
                  });
                })
              )
            ];
        }
      });
    });
  };
  AWS_Cognito.prototype._removeUser = function(user, pool) {
    return __awaiter(this, void 0, void 0, function() {
      var params;
      var _this = this;
      return __generator(this, function(_a) {
        params = {
          UserPoolId: pool.id,
          Username: user.username
        };
        logger_1.logger.info(
          'Removing user "' +
            user.username +
            '" in cognito pool: "' +
            pool.id +
            '"'
        );
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            return __awaiter(_this, void 0, void 0, function() {
              var _this = this;
              return __generator(this, function(_a) {
                this._serviceProvider.adminGetUser(params, function(err, data) {
                  if (!err && data) {
                    _this._serviceProvider.adminDeleteUser(
                      __assign({}, params, { Username: data.Username }),
                      function(err, data) {
                        return err ? reject(err) : resolve(data);
                      }
                    );
                  } else {
                    resolve(data);
                  }
                });
                return [2 /*return*/];
              });
            });
          })
        ];
      });
    });
  };
  AWS_Cognito.prototype._createUser = function(config, pool) {
    return __awaiter(this, void 0, void 0, function() {
      var attributes, params;
      var _this = this;
      return __generator(this, function(_a) {
        attributes = config.attributes || {};
        params = {
          Username: config.username,
          UserAttributes: Object.keys(attributes)
            .map(function(key) {
              return {
                Name: key,
                Value: attributes[key]
              };
            })
            .concat([{ Name: "email_verified", Value: "true" }]),
          DesiredDeliveryMediums: ["EMAIL"],
          TemporaryPassword: this._temporaryPass,
          UserPoolId: pool.id
        };
        logger_1.logger.info(
          'Creating user "' +
            config.username +
            '" in cognito pool: "' +
            pool.id +
            '"'
        );
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            return __awaiter(_this, void 0, void 0, function() {
              var _this = this;
              return __generator(this, function(_a) {
                this._serviceProvider.adminCreateUser(params, function(
                  err,
                  data
                ) {
                  return __awaiter(_this, void 0, void 0, function() {
                    return __generator(this, function(_a) {
                      if (err) {
                        logger_1.logger.error("" + err.stack);
                        process.exit(1);
                      }
                      resolve(
                        __assign({}, config, {
                          username: data.User && data.User.Username
                        })
                      );
                      return [2 /*return*/];
                    });
                  });
                });
                return [2 /*return*/];
              });
            });
          })
        ];
      });
    });
  };
  AWS_Cognito.prototype.addUserToGroup = function(user, pool) {
    return __awaiter(this, void 0, void 0, function() {
      var params;
      var _this = this;
      return __generator(this, function(_a) {
        params = {
          GroupName: user.group || "" /* required */,
          UserPoolId: pool.id /* required */,
          Username: user.username /* required */
        };
        helpers_1.requireProps(params);
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            return __awaiter(_this, void 0, void 0, function() {
              return __generator(this, function(_a) {
                this._serviceProvider.adminAddUserToGroup(params, function(
                  err,
                  data
                ) {
                  if (err) {
                    logger_1.logger.error("" + err.stack);
                    process.exit(1);
                  }
                  resolve(data);
                });
                return [2 /*return*/];
              });
            });
          })
        ];
      });
    });
  };
  AWS_Cognito.prototype.getUserToken = function(user, pool, callback) {
    return __awaiter(this, void 0, void 0, function() {
      var login, databuffer;
      return __generator(this, function(_a) {
        login = child_process_1.spawn(
          "aws cognito-idp initiate-auth         --client-id " +
            pool.clientID +
            "         --auth-flow USER_PASSWORD_AUTH         --auth-parameters         --region us-east-1         USERNAME=" +
            user.username +
            ",PASSWORD=" +
            this._temporaryPass,
          { shell: "/bin/bash" }
        );
        login.on("close", function(code) {
          if (code !== 0) {
            logger_1.logger.error(
              "Command failed with exit code " + code + "."
            );
            process.exit(1);
          } else {
            callback(databuffer);
          }
        });
        databuffer = "";
        login.stdout.on("data", function(data) {
          databuffer += data;
        });
        // print live stderr data to the logger
        login.stderr.on("data", function(data) {
          logger_1.logger.error("stderr: " + data);
        });
        return [2 /*return*/];
      });
    });
  };
  AWS_Cognito.prototype.resetUserPassword = function(user, pool) {
    return __awaiter(this, void 0, void 0, function() {
      var _this = this;
      return __generator(this, function(_a) {
        return [
          2 /*return*/,
          new Promise(function(resolve, reject) {
            _this.getUserToken(user, pool, function(data) {
              logger_1.logger.info(
                "Challenging Password for " + user.attributes.email
              );
              data = JSON.parse(data);
              var changePass = child_process_1.spawn(
                "aws cognito-idp admin-respond-to-auth-challenge         --user-pool-id " +
                  pool.id +
                  "         --client-id " +
                  pool.clientID +
                  '         --challenge-responses         --region us-east-1         "NEW_PASSWORD=' +
                  _this._finalPass +
                  ",USERNAME=" +
                  user.username +
                  '"         --challenge-name NEW_PASSWORD_REQUIRED         --session ' +
                  data.Session,
                { shell: "/bin/bash" }
              );
              changePass.on("close", function(code) {
                if (code !== 0) {
                  logger_1.logger.error(
                    "Command failed with exit code " + code + "."
                  );
                  process.exit(1);
                }
              });
              changePass.stdout.on("data", function() {
                resolve();
              });
              changePass.stderr.on("data", function(data) {
                logger_1.logger.error("stderr: " + data);
                reject();
              });
            });
          })
        ];
      });
    });
  };
  return AWS_Cognito;
})();
exports.AWS_Cognito = AWS_Cognito;
