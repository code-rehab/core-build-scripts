import { AppsyncConfig } from "../../config/aws";
import { AWSContainer } from "../aws";
interface AppsyncTemplateParams {
    route: string;
    [key: string]: string;
}
declare type AppsyncTemplateFunction = (params: AppsyncTemplateParams) => string;
export declare class AWS_Appsync {
    private _aws;
    private _config;
    private _templates;
    constructor(_aws: AWSContainer, config?: Partial<AppsyncConfig>);
    build(config?: Partial<AppsyncConfig>): Promise<void>;
    updateConfig: (_config?: Partial<AppsyncConfig>) => {
        schema: string;
        mapping: string;
        build_path: string;
        templates: {
            request: string;
            response: string;
        } | {
            request: string;
            response: string;
        };
    };
    template(type: "request" | "response", select: string): AppsyncTemplateFunction;
    private _formatFileName;
    private _generateMaps;
    createSchema(schema?: string): string;
}
export {};
