import { AWSContainer } from "./aws";
export declare class AWS_S3 {
    private _aws;
    private _config;
    constructor(_aws: AWSContainer);
    pushAll(config: any[]): Promise<void>;
    pullAll(config: any[]): Promise<void>;
    pull(bucketRef: string, source: string, location?: string): Promise<void>;
    push(bucketRef: string, source: string, location?: string): Promise<void>;
    private _syncbucket;
}
