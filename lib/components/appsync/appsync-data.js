"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppsyncObjectController = /** @class */ (function () {
    function AppsyncObjectController() {
        this._data = {};
    }
    AppsyncObjectController.prototype.get = function () {
        return this._data;
    };
    AppsyncObjectController.prototype.keys = function () {
        return Object.keys(this._data);
    };
    AppsyncObjectController.prototype.toArray = function () {
        var _this = this;
        return this.keys().map(function (key) { return _this._data[key]; });
    };
    AppsyncObjectController.prototype.insert = function (key, value) {
        this._data[key] = value;
        return this;
    };
    return AppsyncObjectController;
}());
exports.AppsyncObjectController = AppsyncObjectController;
var AppsyncSchemaData = /** @class */ (function () {
    function AppsyncSchemaData() {
        this.mapping = new AppsyncObjectController();
        this.models = new AppsyncObjectController();
    }
    return AppsyncSchemaData;
}());
exports.schemaData = new AppsyncSchemaData();
