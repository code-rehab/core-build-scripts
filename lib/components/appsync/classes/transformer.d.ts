import { GraphQLSchema, GraphQLSchemaConfig } from "graphql";
export declare class DefaultTransformer {
    protected _schema: GraphQLSchema | undefined;
    protected _config: GraphQLSchemaConfig & {
        types: any;
    } | undefined;
    pluckType(typename: string): any;
    protected addFields(typename: string, fields: Record<string, any>): void;
}
