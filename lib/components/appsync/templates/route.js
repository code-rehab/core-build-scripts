"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.route_templates = {
  request: {
    resolve: function(_a) {
      var route = _a.route;
      return (
        '\n$util.qr($ctx.stash.put("route", "' +
        route +
        '"))\n$util.qr($ctx.stash.put("operation", "Invoke"))\n\n{}\n'
      );
    },
    resolveBatch: function(_a) {
      var route = _a.route,
        field = _a.field;
      return (
        '\n$util.qr($ctx.stash.put("route", "' +
        route +
        '"))\n$util.qr($ctx.stash.put("operation", "BatchInvoke"))\n$util.qr($ctx.stash.put("field", "' +
        field +
        '"))\n\n{}\n'
      );
    }
  },
  response: {
    resolve: function() {
      return "$utils.toJson($context.result)";
    },
    resolveBatch: function() {
      return "$utils.toJson($context.result)";
    }
  }
};
