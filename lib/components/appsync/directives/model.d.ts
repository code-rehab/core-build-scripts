import { SchemaDirectiveVisitor, Transform } from "graphql-tools";
import { GraphQLObjectType, GraphQLSchema } from "graphql";
import { AppsyncRouteConfig } from "../appsync-data";
import { DefaultTransformer } from "../classes/transformer";
declare enum ResourceFunction {
    Get = "get",
    List = "list",
    Create = "create",
    Update = "update",
    Delete = "delete"
}
export declare class ModelDirective extends SchemaDirectiveVisitor {
    static typeDef(): string;
    static transformer(): ModelTransformer;
    createRoute(functionName: ResourceFunction, type: GraphQLObjectType): AppsyncRouteConfig;
    visitObject(object: GraphQLObjectType): void;
}
declare class ModelTransformer extends DefaultTransformer implements Transform {
    transformSchema(schema: GraphQLSchema): GraphQLSchema;
}
export {};
