import { SchemaDirectiveVisitor } from "graphql-tools";
import { GraphQLField, GraphQLObjectType, GraphQLInterfaceType } from "graphql";
declare type GraphQLFieldDetails = {
    objectType: GraphQLObjectType | GraphQLInterfaceType;
};
export declare class RouteDirective extends SchemaDirectiveVisitor {
    static typeDef(): string;
    visitFieldDefinition(field: GraphQLField<any, any>, details: GraphQLFieldDetails): void;
}
export {};
