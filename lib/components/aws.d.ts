import { AWS_Appsync } from './aws/appsync';
import { AWS_Cognito } from './aws/cognito';
import { AWS_S3 } from './aws/s3';
export declare class AWSContainer {
    private _config;
    private _s3;
    private _cognito;
    private _appsync;
    constructor(region?: string);
    readonly s3: AWS_S3;
    readonly cognito: AWS_Cognito;
    readonly appsync: AWS_Appsync;
    getCFValue: (key: string) => Promise<{}>;
}
