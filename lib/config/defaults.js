"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PWD = process.env.PWD;
exports.build_path = exports.PWD + "/.build/";
var aws_1 = require("./aws");
exports.aws_config = aws_1.aws_config;
