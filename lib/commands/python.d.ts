import { ShellCommand } from "../classes/command-executor";
export interface PythonCommands {
    build: ShellCommand;
}
export declare function pythonCommands(): PythonCommands;
