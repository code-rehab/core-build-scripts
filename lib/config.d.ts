export declare const config: {
    logger: {
        level: string;
    };
    python: {
        build_path: string;
        source: string;
    };
    aws: {
        s3: {
            source: string;
            prefix: string;
        };
    };
};
