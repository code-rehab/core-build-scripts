"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var aws_1 = require("./commands/aws");
var python_1 = require("./commands/python");
exports.python = python_1.pythonCommands();
exports.aws = aws_1.awsCommands();
