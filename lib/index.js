"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value);
            }).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y["return"]
                  : op[0]
                  ? y["throw"] || ((t = y["return"]) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var command_executor_1 = require("./classes/command-executor");
var aws_1 = require("./components/aws/aws");
var python_1 = require("./components/python");
var appsync_1 = require("./components/appsync/appsync");
var env_1 = require("./components/env");
function routine(routineFunction) {
  return __awaiter(this, void 0, void 0, function() {
    return __generator(this, function(_a) {
      switch (_a.label) {
        case 0:
          return [4 /*yield*/, routineFunction()];
        case 1:
          _a.sent();
          return [2 /*return*/];
      }
    });
  });
}
exports.routine = routine;
var config_1 = require("./config");
exports.defaultconfig = config_1.config;
var logger_1 = require("./classes/logger");
exports.logger = logger_1.logger;
var Module = /** @class */ (function() {
  function Module() {
    this._python = undefined;
    this._env = undefined;
    this._appsync = undefined;
    this._aws = undefined;
  }
  Object.defineProperty(Module.prototype, "python", {
    get: function() {
      if (!this._python) {
        this._python = new python_1.Python({});
      }
      return this._python;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(Module.prototype, "env", {
    get: function() {
      if (!this._env) {
        this._env = new env_1.Env();
      }
      return this._env;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(Module.prototype, "appsync", {
    get: function() {
      if (!this._appsync) {
        this._appsync = new appsync_1.AWS_Appsync();
      }
      return this._appsync;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(Module.prototype, "aws", {
    get: function() {
      if (!this._aws) {
        this._aws = new aws_1.AWSContainer();
      }
      return this._aws;
    },
    enumerable: true,
    configurable: true
  });
  return Module;
})();
function commands(commands) {
  var executor = new command_executor_1.CommandExecutor(commands);
  executor.run();
}
exports.commands = commands;
var module = new Module();
exports.default = module;
